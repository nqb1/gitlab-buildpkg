# gitlab-buildpkg

gitlab-buildpkg is used to build container to be used by gitlab-buildpkg-tools

## Documentation / links
* https://major.io/2019/05/24/build-containers-in-gitlab-ci-with-buildah/
* https://major.io/2019/08/13/buildah-error-vfs-driver-does-not-support-overlay-mountopt-options/
* https://gitlab.com/majorhayden/container-buildah
* https://gitlab.com/majorhayden/os-containers
* https://buildah.io/
* https://github.com/containers/buildah
* https://hub.docker.com/r/buildah/buildah
* https://salsa.debian.org/go-team/packages/golang-github-containers-buildah
* https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=928083
* https://developers.redhat.com/blog/2019/08/14/best-practices-for-running-buildah-in-a-container/

## Enter kaniko container
* docker run --entrypoint=/busybox/sh -it gcr.io/kaniko-project/executor:debug
