# The problem on gitlab-runner
On a ubuntu Runner I had the issue
https://github.com/containers/buildah/issues/1901

## Test the bug

With:
```
sudo docker run --rm -it quay.io/buildah/stable
unshare -r
unshare: unshare failed: Operation not permitted
```

Without:
```
# https://github.com/containers/libpod/blob/master/seccomp.json
wget https://raw.githubusercontent.com/containers/libpod/master/seccomp.json
sudo docker run --security-opt seccomp=$(pwd)/seccomp.json --rm -it quay.io/buildah/stable
unshare -r
```

I hadn't to, but you should test, just in case :
```
echo 1 > /proc/sys/kernel/unprivileged_userns_clone
```

## Todo on the Gitlab Runner

* https://gitlab.com/gitlab-org/gitlab-runner/issues/2597
* Here's Docker's default seccomp profile: https://github.com/moby/moby/blob/master/profiles/seccomp/default.json
* Here: https://docs.docker.com/engine/reference/run/#security-configuration are listed all the docker run currently supported security configurations.
* https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/21
* About seccomp=unconfined https://github.com/containers/buildah/issues/1469

Dans /etc/gitlab-runner/config.toml

```
[runners.docker]
   #security_opt = ["seccomp:/etc/gitlab-runner/seccomp.json"]
   security_opt = ["seccomp:unconfined"]
```

* About vfs problem : https://major.io/2019/08/13/buildah-error-vfs-driver-does-not-support-overlay-mountopt-options/ Add:

```
sed -i '/^mountopt =.*/d' /etc/containers/storage.conf
```

* Also https://github.com/containers/buildah/blob/master/troubleshooting.md
* full k8s example https://gist.github.com/tomkukral/7d94eb60774d351433036f3cc4546aad
* https://major.io/2019/05/24/build-containers-in-gitlab-ci-with-buildah/
* https://github.com/containers/buildah/issues/1387


## More Todo on the Gitlab Runner

* https://github.com/containers/buildah/tree/master/contrib/buildahimage
* Use image quay.io/buildah/stable:latest 
* Add "label=disable" in runner security options

```
[runners.docker]
   security_opt = ["seccomp:unconfined", "label=disable"]
```

## Kaniko 

* https://github.com/GoogleContainerTools/kaniko
* https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
